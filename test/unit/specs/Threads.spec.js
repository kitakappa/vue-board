import Vue from 'vue'
import Threads from '@/components/Threads'

import sinon from 'sinon'
import api from '@/lib/api'
import router from '@/router'
var getThreadsStub

describe('Threads.vue', () => {
  before(() => {
    getThreadsStub = sinon.stub(api, 'getThreads').callsFake(() => {
      return Promise.resolve([{
        'id': 1,
        'name': 'hogefuga'
      },
      {
        'id': 2,
        'name': 'スレッド名だよ'
      }
      ])
    })
  })
  after(() => {
    getThreadsStub.restore()
  })
  it('should render correct contents', done => {
    const Constructor = Vue.extend(Threads)
    const vm = new Constructor({router}).$mount()
    Vue.nextTick(() => {
      Vue.nextTick(() => {
        Vue.nextTick(() => {
          expect(vm.$el.querySelectorAll('li.thread').length).to.equal(2)
          done()
        })
      })
    })
  })
})
