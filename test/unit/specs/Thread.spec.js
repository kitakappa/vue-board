import Vue from 'vue'
import Thread from '@/components/Thread'

import sinon from 'sinon'
import api from '@/lib/api'
import router from '@/router'
var getCommentsStub, getThreadStub

describe('Thread.vue', () => {
  before(() => {
    getCommentsStub = sinon.stub(api, 'getComments').callsFake(() => {
      return Promise.resolve([
        {
          'threadId': 1,
          'id': 1,
          'body': 'some comment 0',
          'postId': 1,
          'createdBy': 'fuga',
          'createdAt': '2017-07-08T16:20:25.950Z'
        },
        {
          'threadId': 2,
          'id': 100,
          'body': 'some comment3',
          'postId': 1,
          'createdBy': 'fuga',
          'createdAt': '2017-07-08T16:20:25.950Z'
        }])
    })
    getThreadStub = sinon.stub(api, 'getThread').callsFake(() => {
      return Promise.resolve({
        'id': 1,
        'name': 'hogefuga'
      })
    })
  })
  after(() => {
    getCommentsStub.restore()
    getThreadStub.restore()
  })
  it('should render correct contents', done => {
    const Constructor = Vue.extend(Thread)
    const vm = new Constructor({router}).$mount()
    Vue.nextTick(() => {
      Vue.nextTick(() => {
        Vue.nextTick(() => {
          expect(vm.$el.querySelectorAll('h4')).to.have.lengthOf(2)
          expect(vm.$el.querySelector('h1').textContent).to.equal('hogefuga')
          done()
        })
      })
    })
  })
})
