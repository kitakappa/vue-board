const axios = require('axios')
const baseUrl = 'http://localhost:3004'

export default {
  getThreads: () => axios.get(baseUrl + '/threads')
    .then(res => res.data),
  getThread: id => axios.get(baseUrl + '/threads/' + id)
    .then(res => res.data),
  getComments: threadId => axios.get(baseUrl + '/comments?threadId=' + threadId)
    .then(res => res.data),
  deleteComment: id => axios.delete(baseUrl + '/comments/' + id)
    .then(res => res.data),
  saveComment: params => axios.post(baseUrl + '/comments/', params)
    .then(res => res.data)
}
